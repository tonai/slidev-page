# Gitlab pages slidev

How to build slidev for Gitlab pages ?

---

## My `.gitlab-ci.yml`

```yml
image: node:slim

workflow:
  rules:
    - if: $CI_COMMIT_BRANCH

pages:
  stage: deploy
  script:
    - npm ci
    - npm run build
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```

---

## My `package.json`

```json
{
  "name": "slidev-page",
  "scripts": {
    "dev": "slidev",
    "build": "slidev build --out public --base /slidev-page"
  },
  "devDependencies": {
    "@slidev/cli": "^0.32.3"
  },
  "dependencies": {
    "@slidev/theme-default": "^0.21.2"
  }
}
```
